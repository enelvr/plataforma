<?php
require_once 'conexion.php';
class Analysi{
    const TABLA = 'analysis';
    public $id;
    public $code;
    public $date;
    public $place;
    public $description;
    public $dangercheck1;
    public $dangercheck2;
    public $dangercheck3;
    public $dangercheck4;
    public $dangercheck5;
    public $dangercheck6;
    public $dangercheck7;
    public $dangercheck8;
    public $dangercheck9;
    public $dangercheck10;
    public $dangercheck11;
    public $dangercheck12;
    public $dangercheck13;
    public $dangercheck14;
    public $description_work;
    public $dangers;
    public $controls;
    public $responsable;
    public $protectioncheck1;
    public $protectioncheck2;
    public $protectioncheck3;
    public $protectioncheck4;
    public $protectioncheck5;
    public $protectioncheck6;
    public $protectioncheck7;
    public $protectioncheck8;
    public $protectioncheck9;
    public $protectioncheck10;
    public $protectioncheck11;
    public $measures;

    public function set($attribute, $content){
        $this->$attribute = $contenido;
    }
    public function get($attribute){
        return $this->$attribute;
    }
    public function __construct($code, $date, $place, $description, $dangercheck1, $dangercheck2, $dangercheck3, $dangercheck4, $dangercheck5, $dangercheck6,$dangercheck7,$dangercheck8,$dangercheck9,$dangercheck10,$dangercheck11,$dangercheck12,$dangercheck13, $dangercheck14,
    $description_work, $dangers, $controls, $responsable, $protectioncheck1, $protectioncheck2, $protectioncheck3, $protectioncheck4,$protectioncheck5,$protectioncheck6,$protectioncheck7,$protectioncheck8,$protectioncheck9,$protectioncheck10,$protectioncheck11,
    $measures, $id=null){
        $this->code =$code;
        $this->date=$date;
        $this->place=$place;
        $this->description=$description;
        $this->dangercheck1=$dangercheck1;
        $this->dangercheck2=$dangercheck2;
        $this->dangercheck3=$dangercheck3;
        $this->dangercheck4=$dangercheck4;
        $this->dangercheck5=$dangercheck5;
        $this->dangercheck6=$dangercheck6;
        $this->dangercheck7=$dangercheck7;
        $this->dangercheck8=$dangercheck8;
        $this->dangercheck9=$dangercheck9;
        $this->dangercheck10=$dangercheck10;
        $this->dangercheck11=$dangercheck11;
        $this->dangercheck12=$dangercheck12;
        $this->dangercheck13=$dangercheck13;
        $this->dangercheck14=$dangercheck14;
        $this->description_work=$description_work;
        $this->dangers=$dangers;
        $this->controls=$controls;
        $this->responsable=$responsable;
        $this->protectioncheck1=$protectioncheck1;
        $this->protectioncheck2=$protectioncheck2;
        $this->protectioncheck3=$protectioncheck3;
        $this->protectioncheck4=$protectioncheck4;
        $this->protectioncheck5=$protectioncheck5;
        $this->protectioncheck6=$protectioncheck6;
        $this->protectioncheck7=$protectioncheck7;
        $this->protectioncheck8=$protectioncheck8;
        $this->protectioncheck9=$protectioncheck9;
        $this->protectioncheck10=$protectioncheck10;
        $this->protectioncheck11=$protectioncheck11;
        $this->measures=$measures;
        $this->id=$id;
    }

    public function save(){
        $conexion = new Conexion();
        $consulta = $conexion->prepare('INSERT INTO ' . self::TABLA .' (code, date, place, description, dangercheck1, dangercheck2, dangercheck3, dangercheck4, dangercheck5, dangercheck6, dangercheck7, dangercheck8, dangercheck9, dangercheck10, dangercheck11, dangercheck12, dangercheck13, dangercheck14,
        description_work,dangers, controls, responsable, protectioncheck1, protectioncheck2, protectioncheck3, protectioncheck4, protectioncheck5, protectioncheck6, protectioncheck7, protectioncheck8, protectioncheck9, protectioncheck10, protectioncheck11,
        measures) VALUES (:code, :date, :place, :description, :dangercheck1, :dangercheck2, :dangercheck3, :dangercheck4, :dangercheck5, :dangercheck6,:dangercheck7, :dangercheck8, :dangercheck9, :dangercheck10, :dangercheck11, :dangercheck12, :dangercheck13, :dangercheck14,
        :description_work, :dangers, :controls, :responsable, :protectioncheck1, :protectioncheck2, :protectioncheck3, :protectioncheck4, :protectioncheck5, :protectioncheck6, :protectioncheck7, :protectioncheck8, :protectioncheck9, :protectioncheck10, :protectioncheck11,
        :measures)');
        $consulta->bindParam(':code', $this->code); $consulta->bindParam(':date',$this->date); $consulta->bindParam(':place',$this->place);
        $consulta->bindParam(':description',$this->description); $consulta->bindParam(':dangercheck1',$this->dangercheck1);
        $consulta->bindParam(':dangercheck2',$this->dangercheck2);
        $consulta->bindParam(':dangercheck3',$this->dangercheck3); $consulta->bindParam(':dangercheck4',$this->dangercheck4);
        $consulta->bindParam(':dangercheck5',$this->dangercheck5); $consulta->bindParam(':dangercheck6',$this->dangercheck6);
        $consulta->bindParam(':dangercheck7',$this->dangercheck7); $consulta->bindParam(':dangercheck8',$this->dangercheck8);
        $consulta->bindParam(':dangercheck9',$this->dangercheck9); $consulta->bindParam(':dangercheck10',$this->dangercheck10);
        $consulta->bindParam(':dangercheck11',$this->dangercheck11); $consulta->bindParam(':dangercheck12',$this->dangercheck12);
        $consulta->bindParam(':dangercheck13',$this->dangercheck13);
        $consulta->bindParam(':dangercheck14',$this->dangercheck14);
        $consulta->bindParam(':description_work',$this->description_work);
        $consulta->bindParam(':dangers',$this->dangers); $consulta->bindParam(':controls',$this->controls);
        $consulta->bindParam(':responsable',$this->responsable); $consulta->bindParam(':protectioncheck1',$this->protectioncheck1);
        $consulta->bindParam(':protectioncheck2',$this->protectioncheck2); $consulta->bindParam(':protectioncheck3',$this->protectioncheck3);
        $consulta->bindParam(':protectioncheck4',$this->protectioncheck4); $consulta->bindParam(':protectioncheck5',$this->protectioncheck5);
        $consulta->bindParam(':protectioncheck6',$this->protectioncheck6); $consulta->bindParam(':protectioncheck7',$this->protectioncheck7);
        $consulta->bindParam(':protectioncheck8',$this->protectioncheck8); $consulta->bindParam(':protectioncheck9',$this->protectioncheck9);
        $consulta->bindParam(':protectioncheck10',$this->protectioncheck10); $consulta->bindParam(':protectioncheck11',$this->protectioncheck11);
        $consulta->bindParam(':measures',$this->measures);
        $consulta->execute();
        $this->id = $conexion->lastInsertId();
        $conexion = null;
    }
    public static function find($id){
        $conexion = new Conexion();
        $consulta = $conexion->prepare('SELECT *  FROM ' . self::TABLA . ' WHERE id = :id');
        $consulta->bindParam(':id', $id);
        $consulta->execute();
        $registro = $consulta->fetch();
        if($registro){
            return new self($registro['code'], $registro['date'], $registro['place'], $registro['description'], $registro['dangercheck1'],
            $registro['dangercheck2'],$registro['dangercheck3'],$registro['dangercheck4'],$registro['dangercheck5'],$registro['dangercheck6'],
            $registro['dangercheck7'],$registro['dangercheck8'],$registro['dangercheck9'],$registro['dangercheck10'],$registro['dangercheck11'],
            $registro['dangercheck12'],$registro['dangercheck13'],$registro['dangercheck14'], $registro['description_work'], $registro['dangers'], $registro['controls'],
            $registro['responsable'],  $registro['protectioncheck1'], $registro['protectioncheck2'], $registro['protectioncheck3'],
            $registro['protectioncheck4'], $registro['protectioncheck5'], $registro['protectioncheck6'], $registro['protectioncheck7'],
            $registro['protectioncheck8'], $registro['protectioncheck9'], $registro['protectioncheck10'], $registro['protectioncheck11'],
            $registro['measures'], $registro['id'],  $id);
        }else{
            return false;
        }

    }

}

 ?>
