CREATE TABLE `analysis` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dangercheck1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck13` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dangercheck14` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_work` text COLLATE utf8mb4_unicode_ci,
  `dangers` text COLLATE utf8mb4_unicode_ci,
  `controls` text COLLATE utf8mb4_unicode_ci,
  `responsable` text COLLATE utf8mb4_unicode_ci,
  `protectioncheck1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `protectioncheck11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `measures` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `incharges` (
  `id` int(10) UNSIGNED NOT NULL,
  `analysi_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `charge` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



INSERT INTO `types` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'ELABORACION', NULL, NULL),
(2, 'APROBACION', NULL, NULL),
(3, 'DIVULGACION', NULL, NULL);


ALTER TABLE `analysis`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `incharges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incharges_analysi_id_foreign` (`analysi_id`),
  ADD KEY `incharges_type_id_foreign` (`type_id`);

ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `analysis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `incharges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;


ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `incharges`
  ADD CONSTRAINT `incharges_analysi_id_foreign` FOREIGN KEY (`analysi_id`) REFERENCES `analysis` (`id`),
  ADD CONSTRAINT `incharges_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);
