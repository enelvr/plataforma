<?php require_once 'Analysi.php';?>
<?php require_once 'Incharge.php';?>
<?php $id=$_GET['id'];?>
<?php $analysi = Analysi::find($id);?>
<?php if(!$analysi){header("Location: create.php");}?>
<?php $elaboracion = Incharge::find($id);?>
<?php $aprobacion = Incharge::findA($id);?>
<?php $divulgacion = Incharge::findD($id);?>
<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	  <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
		<header>
			<nav></nav>
		</header>
	<div id="main">
		<section id="content">
<div class="row">
	<div class="col s12 m12 l12">
		<div class="card-panel">
            id:<?php echo $analysi->id; ?><br>
            cdodigo:<?php echo $analysi->code;?><br>
			fecha:<?php echo $analysi->date;?><br>
			lugar:<?php echo $analysi->place;?><br>
			medidas:<?php echo $analysi->measures;?><br>
			peligros:<?php echo $analysi->dangers;?><br>
			controles:<?php echo $analysi->controls;?><br>
			responsables:<?php echo $analysi->responsable;?><br>

			<div class="row">
			   <div class="col s12 m12 l12">
				   <h5>IDENTIFICACIÓN DE PELIGROS/ASPECTOS</h5>
				   <div class="col s4">
					   <p>
							 <input type="checkbox" id="dangercheck1" name="dangercheck1" <?php echo $analysi->dangercheck1;?> disabled="disabled"/>
							 <label for="dangercheck1">QUIMICAS: Exposicion o contacto con sustancias quimicas</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck2" name="dangercheck2"  <?php echo $analysi->dangercheck2;?> disabled="disabled"/>
							 <label for="dangercheck2">FISICO QUIMICO</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck3" name="dangercheck3"  <?php echo $analysi->dangercheck3;?> disabled="disabled"/>
							 <label for="dangercheck3">ELECTRICO: Exposicion o contacto con electricidad</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck4" name="dangercheck4" <?php echo $analysi->dangercheck4;?> disabled="disabled" />
							 <label for="dangercheck4">FISICO:Exposicion a ruido</label>
						   </p>
					   </div>
					   <div class="col s4">
						   <p>
							 <input type="checkbox" id="dangercheck5" name="dangercheck5"  <?php echo $analysi->dangercheck5;?> disabled="disabled"/>
							 <label for="dangercheck5">BIOLOGICO</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck6" name="dangercheck6" <?php echo $analysi->dangercheck6;?> disabled="disabled" />
							 <label for="dangercheck6">FISICO: Exposicion a temperaturas ex...</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck7" name="dangercheck7" <?php echo $analysi->dangercheck7;?> disabled="disabled" />
							 <label for="dangercheck7">MECANICOS</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck8" name="dangercheck8" <?php echo $analysi->dangercheck8;?> disabled="disabled" />
							 <label for="dangercheck8">Otros</label>
						   </p>
					   </div>
					   <div class="col s4">
						   <p>
							 <input type="checkbox" id="dangercheck9" name="dangercheck9" <?php echo $analysi->dangercheck9;?> disabled="disabled" />
							 <label for="dangercheck9">ASPECTO AMBIENTAL</label>
						   </p>
					   <p>
							 <input type="checkbox" id="dangercheck10" name="dangercheck10" <?php echo $analysi->dangercheck10;?> disabled="disabled"/>
							 <label for="dangercheck10">NATURALES</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck11" name="dangercheck11" <?php echo $analysi->dangercheck11;?> disabled="disabled" />
							 <label for="dangercheck11">ALTO RIESGO</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck12" name="dangercheck12" <?php echo $analysi->dangercheck12;?> disabled="disabled" />
							 <label for="dangercheck12">LOCATIVOS</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck13" name="dangercheck13" <?php echo $analysi->dangercheck13;?> disabled="disabled"  />
							 <label for="dangercheck13">ERGONOMICO Sobreesfuerzo, posiciones prolongadas</label>
						   </p>
						   <p>
							 <input type="checkbox" id="dangercheck14" name="dangercheck14" <?php echo $analysi->dangercheck14;?> disabled="disabled"/>
							 <label for="dangercheck14">TRANSPORTE</label>
						   </p>
					   </div>
			   </div>
			</div>
			<hr>
			<div class="row">
			   <div class="col s12 m12 l12">
				   <h5>EQUIPOS DE PROTECCIÓN</h5>
				   <div class="row">
			   <div class="col s4">
					<p>
					   <input type="checkbox" id="protectioncheck1" name="protectioncheck1" <?php echo $analysi->protectioncheck1;?> disabled="disabled"/>
						 <label for="protectioncheck1">Dotación jean- camiseta</label>
				   </p>
					<p>
					   <input type="checkbox" id="protectioncheck2" name="protectioncheck2" <?php echo $analysi->protectioncheck2;?> disabled="disabled"/>
						 <label for="protectioncheck2">Careta o Visor</label>
				   </p>
					<p>
					   <input type="checkbox" id="protectioncheck3"name="protectioncheck3" <?php echo $analysi->protectioncheck3;?> disabled="disabled" />
						 <label for="protectioncheck3">Guantes</label>
				   </p>
						   </div>
						   <div class="col s4">
					<p>
					   <input type="checkbox" id="protectioncheck4" name="protectioncheck4" <?php echo $analysi->protectioncheck4;?> disabled="disabled"/>
					   <label for="protectioncheck4">Casco de seguridad</label>
				   </p>
				   <p>
					   <input type="checkbox" id="protectioncheck5" name="protectioncheck5" <?php echo $analysi->protectioncheck5;?> disabled="disabled"/>
					   <label for="protectioncheck5">Botas de seguridad</label>
				   </p>
				   <p>
					   <input type="checkbox" id="protectioncheck6" name="protectioncheck6" <?php echo $analysi->protectioncheck6;?> disabled="disabled"/>
					   <label for="protectioncheck6">Proteccion visual</label>
				   </p>
						   </div>
						   <div class="col s4">
					<p>
					   <input type="checkbox" id="protectioncheck7" name="protectioncheck7" <?php echo $analysi->protectioncheck7;?> disabled="disabled"/>
					   <label for="protectioncheck7">Proteccion respiratoria</label>
				   </p>
				   <p>
					   <input type="checkbox" id="protectioncheck8" name="protectioncheck8" <?php echo $analysi->protectioncheck8;?> disabled="disabled" />
					   <label for="protectioncheck8">Proteccion auditiva(tipo tapon)</label>
				   </p>
				   <p>
					   <input type="checkbox" id="protectioncheck9" name="protectioncheck9" <?php echo $analysi->protectioncheck9;?> disabled="disabled"/>
					   <label for="protectioncheck9">Peto</label>
				   </p>
				   <p>
					   <input type="checkbox" id="protectioncheck10" name="protectioncheck10" <?php echo $analysi->protectioncheck10;?> disabled="disabled" />
					   <label for="protectioncheck10">Taivek</label>
				   </p>
				   <p>
					   <input type="checkbox" id="protectioncheck11" name="protectioncheck11" <?php echo $analysi->protectioncheck11;?> disabled="disabled"/>
					   <label for="protectioncheck11">Overol</label>
				   </p>
						   </div>

			</div>

			   </div>
			</div>
			<?php if ($elaboracion):?>
			<h5>ELABORACION</h5>
			<table>
				<thead>
					<tr>
						<td>CARGO</td>
						<td>NOMBRE</td>
						<td>FIRMA</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($elaboracion as $i => $e):?>
					<tr>
						<td><?php echo $e['charge'];?></td>
						<td><?php echo $e['name'];?></td>
						<td><?php echo $e['firm'];?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php endif;?>
			<?php if ($aprobacion):?>
			<h5>APROBACION</h5>
			<table>
				<thead>
					<tr>
						<td>CARGO</td>
						<td>NOMBRE</td>
						<td>FIRMA</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($aprobacion  as $KEY => $a):?>
					<tr>
						<td><?php echo $a['charge'];?></td>
						<td><?php echo $a['name'];?></td>
						<td><?php echo $a['firm'];?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php endif;?>
			<?php if ($divulgacion):?>
			<h5>DIVULGACION</h5>
			<table>
				<thead>
					<tr>
						<td>CARGO</td>
						<td>NOMBRE</td>
						<td>FIRMA</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($divulgacion  as $KEY => $d):?>
					<tr>
						<td><?php echo $d['charge'];?></td>
						<td><?php echo $d['name'];?></td>
						<td><?php echo $d['firm'];?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif;?>
        </div>
		<div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
		    <a href="create.php "class="btn-floating btn-large" title="Nuevo Analisis">
		   	  	<i class="material-icons">add</i>
			</a>
		</div>
	</div>
	</div>
</section>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
</body>
</html>
