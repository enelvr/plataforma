
<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	  <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
		<header>
			<nav></nav>
		</header>
	<div id="main">
		<section id="content">
<div class="row">
	<div class="col s12 m12 l12">
		<div class="card-panel">
			<form method="POST" action="AnalysisController.php">
				<div class="row">
					<h5>ANALISIS DE RIESGO POR OFICIO
(ARO)</h5>
					<div class="input-field col s6">
						<div class="form-group label-floating">
					                    <label class="control-label">FECHA</label>
					                    <input type="text" class="datepicker" name="date" required="required">
					              </div>
				                </div>
					<div class="input-field col s6">
						<div class="form-group label-floating">
					                    <label class="control-label">LUGAR</label>
					                    <input type="text" class="form-control" name="place" required="required">
					                </div>
					</div>
				 </div>
				 <div class="row">
					<div class="form-group label-floating">
					             <label class="control-label">DESCRIPCION Y ALCANCE DEL TRABAJO</label>
					              <input type="text" class="form-control" name="description" required="required">
					</div>
				 </div>
				 <div class="row">
				 	<div class="col s12 m12 l12">
				 		<h5>IDENTIFICACIÓN DE PELIGROS/ASPECTOS</h5>
				 		<div class="col s4">
							<p>
							      <input type="checkbox" id="dangercheck1" name="dangercheck1" value="checked" />
							      <label for="dangercheck1">QUIMICAS: Exposicion o contacto con sustancias quimicas</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck2" name="dangercheck2" value="checked" />
							      <label for="dangercheck2">FISICO QUIMICO</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck3" name="dangercheck3" value="checked"  />
							      <label for="dangercheck3">ELECTRICO: Exposicion o contacto con electricidad</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck4" name="dangercheck4" value="checked" />
							      <label for="dangercheck4">FISICO:Exposicion a ruido</label>
							    </p>
						    </div>
						    <div class="col s4">
							    <p>
							      <input type="checkbox" id="dangercheck5" name="dangercheck5" value="checked" />
							      <label for="dangercheck5">BIOLOGICO</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck6" name="dangercheck6" value="checked"  />
							      <label for="dangercheck6">FISICO: Exposicion a temperaturas ex...</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck7" name="dangercheck7" value="checked"  />
							      <label for="dangercheck7">MECANICOS</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck8" name="dangercheck8" value="checked" />
							      <label for="dangercheck8">Otros</label>
							    </p>
						    </div>
						    <div class="col s4">
						    	<p>
							      <input type="checkbox" id="dangercheck9" name="dangercheck9" value="checked" />
							      <label for="dangercheck9">ASPECTO AMBIENTAL</label>
							    </p>
							<p>
							      <input type="checkbox" id="dangercheck10" name="dangercheck10" value="checked" />
							      <label for="dangercheck10">NATURALES</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck11" name="dangercheck11" value="checked"  />
							      <label for="dangercheck11">ALTO RIESGO</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck12" name="dangercheck12" value="checked" />
							      <label for="dangercheck12">LOCATIVOS</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck13" name="dangercheck13" value="checked" />
							      <label for="dangercheck13">ERGONOMICO Sobreesfuerzo, posiciones prolongadas</label>
							    </p>
							    <p>
							      <input type="checkbox" id="dangercheck14" name="dangercheck14" value="checked" />
							      <label for="dangercheck14">TRANSPORTE</label>
							    </p>
						    </div>
				 	</div>
				 </div>
				 <hr>
				 <div class="row">
				 	<div class="input-field col s6">
						<div class="form-group label-floating">
					                    <label class="control-label">Descripción del trabajo (Paso a paso)</label>
					                    <input type="text" class="form-control" name="description_work">
					              </div>
				                </div>
				                <div class="input-field col s6">
						<div class="form-group label-floating">
					                    <label class="control-label">Peligros</label>
					                    <input type="text" class="form-control" name="dangers">
					              </div>
				                </div>
				 </div>
				  <div class="row">
				 	<div class="input-field col s6">
						<div class="form-group label-floating">
					                    <label class="control-label">Controles</label>
					                    <input type="text" class="form-control" name="controls">
					              </div>
				                </div>
				                <div class="input-field col s6">
						<div class="form-group label-floating">
					                    <label class="control-label">Responsable</label>
					                    <input type="text" class="form-control" name="responsable">
					              </div>
				                </div>
				 </div><hr>
				 <div class="row">
				 	<div class="col s12 m12 l12">
				 		<h5>EQUIPOS DE PROTECCIÓN</h5>
						<div class="row">
				 	<div class="col s4">
						 <p>
							<input type="checkbox" id="protectioncheck1" name="protectioncheck1" value="checked"/>
							  <label for="protectioncheck1">Dotación jean- camiseta</label>
						</p>
						 <p>
							<input type="checkbox" id="protectioncheck2" name="protectioncheck2" value="checked"/>
							  <label for="protectioncheck2">Careta o Visor</label>
						</p>
						 <p>
							<input type="checkbox" id="protectioncheck3"name="protectioncheck3" value="checked" />
							  <label for="protectioncheck3">Guantes</label>
						</p>
				                </div>
				                <div class="col s4">
						 <p>
							<input type="checkbox" id="protectioncheck4" name="protectioncheck4" value="checked"/>
							<label for="protectioncheck4">Casco de seguridad</label>
						</p>
						<p>
							<input type="checkbox" id="protectioncheck5" name="protectioncheck5" value="checked"/>
							<label for="protectioncheck5">Botas de seguridad</label>
						</p>
						<p>
							<input type="checkbox" id="protectioncheck6" name="protectioncheck6" value="checked"/>
							<label for="protectioncheck6">Proteccion visual</label>
						</p>
				                </div>
				                <div class="col s4">
						 <p>
							<input type="checkbox" id="protectioncheck7" name="protectioncheck7" value="checked"/>
							<label for="protectioncheck7">Proteccion respiratoria</label>
						</p>
						<p>
							<input type="checkbox" id="protectioncheck8" name="protectioncheck8" value="checked" />
							<label for="protectioncheck8">Proteccion auditiva(tipo tapon)</label>
						</p>
						<p>
							<input type="checkbox" id="protectioncheck9" name="protectioncheck9" value="checked"/>
							<label for="protectioncheck9">Peto</label>
						</p>
						<p>
							<input type="checkbox" id="protectioncheck10" name="protectioncheck10" value="checked" />
							<label for="protectioncheck10">Taivek</label>
						</p>
						<p>
							<input type="checkbox" id="protectioncheck11" name="protectioncheck11" value="checked"/>
							<label for="protectioncheck11">Overol</label>
						</p>
				                </div>

				 </div>

				 	</div>
				 </div>
				  <div class="row">
				 	<div class="col s12 m12 l12">
				 		<h5>MEDIDAS  / REQUERIMIENTOS DE SEGURIDAD ADICIONALES</h5>
						<div class="form-group label-floating">
					                    <label class="control-label">MEDIDAS</label>
					                    <input type="text" class="form-control" name="measures">
					              </div>
				 	</div>
				 </div>
				 <div class="row">
				 	<h5>ELABORACION</h5>
						<div class="row">
							<div class="input-field col s4">
								<div class="form-group label-floating">
							        <label class="control-label">CARGO</label>
							        <input type="text" class="form-control" name="charge1">
							    </div>
						    </div>
						    <div class="input-field col s4">
								<div class="form-group label-floating">
							        <label class="control-label">NOMBRE</label>
							        <input type="text" class="form-control" name="name1">
							     </div>
						    </div>
						    <div class="input-field col s4">
								<div class="form-group label-floating">
							        <label class="control-label">FIRMA</label>
							        <input type="text" class="form-control" name="firm1">
							</div>
						</div>
				 		<div class="input-field col s4">
							<div class="form-group label-floating">
					            <label class="control-label">CARGO</label>
					            <input type="text" class="form-control" name="charge2">
					        </div>
				        </div>
				        <div class="input-field col s4">
							<div class="form-group label-floating">
					            <label class="control-label">NOMBRE</label>
					            <input type="text" class="form-control" name="name2">
					         </div>
				        </div>
				        <div class="input-field col s4">
							<div class="form-group label-floating">
					            <label class="control-label">FIRMA</label>
					            <input type="text" class="form-control" name="firm2">
					        </div>
				        </div>
				 </div>
				<div class="row">
				 	<h5>APROBACION</h5>
					<div class="row">
						<div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">CARGO</label>
						        <input type="text" class="form-control" name="charge3">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">NOMBRE</label>
						        <input type="text" class="form-control" name="name3">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">FIRMA</label>
						        <input type="text" class="form-control" name="firm3">
						    </div>
					    </div>
					</div>
				 	<div class="input-field col s4">
						<div class="form-group label-floating">
					        <label class="control-label">CARGO</label>
					        <input type="text" class="form-control" name="charge4">
					     </div>
				    </div>
				    <div class="input-field col s4">
						<div class="form-group label-floating">
					        <label class="control-label">NOMBRE</label>
					        <input type="text" class="form-control" name="name4">
					     </div>
				    </div>
				    <div class="input-field col s4">
						<div class="form-group label-floating">
					        <label class="control-label">FIRMA</label>
					        <input type="text" class="form-control" name="firm4">
					    </div>
				    </div>
				 </div>
				 <div class="row">
				 	<h5>DIVULGACION</h5>
					<div class="row">
						<div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">CARGO</label>
						        <input type="text" class="form-control" name="charge5">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">NOMBRE</label>
						        <input type="text" class="form-control" name="name5">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">FIRMA</label>
						        <input type="text" class="form-control" name="firm5">
						         </div>
					    </div>
					</div>
					<div class="row">
						<div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">CARGO</label>
						        <input type="text" class="form-control" name="charge6">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">NOMBRE</label>
						        <input type="text" class="form-control" name="name6">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">FIRMA</label>
						        <input type="text" class="form-control" name="firm6">
						         </div>
					    </div>
					</div>
					<div class="row">
						<div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">CARGO</label>
						        <input type="text" class="form-control" name="charge7">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">NOMBRE</label>
						        <input type="text" class="form-control" name="name7">
						     </div>
					    </div>
					    <div class="input-field col s4">
							<div class="form-group label-floating">
						        <label class="control-label">FIRMA</label>
						        <input type="text" class="form-control" name="firm7">
						         </div>
					    </div>
					</div>
				 	<div class="input-field col s4">
						<div class="form-group label-floating">
					        <label class="control-label">CARGO</label>
					        <input type="text" class="form-control" name="charge8">
					     </div>
				    </div>
				    <div class="input-field col s4">
						<div class="form-group label-floating">
					        <label class="control-label">NOMBRE</label>
					        <input type="text" class="form-control" name="name8">
					     </div>
				    </div>
				    <div class="input-field col s4">
						<div class="form-group label-floating">
					        <label class="control-label">FIRMA</label>
					        <input type="text" class="form-control" name="firm8">
					         </div>
				    </div>
				 </div>
			 <div class="form-group text-right">
			            <a href="#">Regresar al Inicio</a>
			            <input type="submit" class="btn btn-primary btn-sm" value="Guardar">
       			</div>
			</form>
		</div>
	</div>
	</div>
</section>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
    <script type="text/javascript">
    	$('.datepicker').pickadate({
    	selectMonths: true, // Creates a dropdown to control month
    	selectYears: 15, // Creates a dropdown of 15 years to control year,
    	today: 'Today',
    	clear: 'Clear',
    	close: 'Ok',
    	format: 'yyyy-mm-dd',
    	closeOnSelect: false // Close upon selecting a date,
    	});
    </script>
</body>
</html>
