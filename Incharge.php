<?php
require_once 'conexion.php';
class Incharge{
    const TABLA = 'incharges';
    public $id;
    public $analysi_id;
    public $type_id;
    public $charge;
    public $name;
    public $firm;

    public function set($attribute, $content){
        $this->$attribute = $contenido;
    }
    public function get($attribute){
        return $this->$attribute;
    }
    public function __construct ($analysi_id, $type_id, $charge, $name, $firm, $id=null) {

		$this->analysi_id = $analysi_id;
		$this->type_id = $type_id;
		$this->charge = $charge;
		$this->name= $name;
		$this->firm = $firm;
		$this->id = $id;
	}

    public function save() {
        $conexion = new Conexion();
        if(!empty($this->charge) && !empty($this->name) && !empty($this->firm)){
        $consulta = $conexion->prepare('INSERT INTO ' . self::TABLA .' (analysi_id, type_id, charge, name, firm) VALUES(:analysi_id, :type_id, :charge, :name, :firm)');
        $consulta->bindParam(':analysi_id', $this->analysi_id);
        $consulta->bindParam(':type_id', $this->type_id);
        $consulta->bindParam(':charge', $this->charge);
        $consulta->bindParam(':name', $this->name);
        $consulta->bindParam(':firm', $this->firm);
        $consulta->execute();
        $this->id = $conexion->lastInsertId();
        }
        $conexion = null;
    }
    public static function find($id){
        $conexion = new Conexion();
        $consulta = $conexion->prepare('SELECT *  FROM ' . self::TABLA . ' WHERE analysi_id = :id AND type_id =1 ORDER BY id');
        $consulta->bindParam(':id', $id);
        $consulta->execute();
        $register = $consulta->fetchAll();
        return $register;
    }
    public static function findA($id){
        $conexion = new Conexion();
        $consulta = $conexion->prepare('SELECT *  FROM ' . self::TABLA . ' WHERE analysi_id = :id AND type_id =2 ORDER BY id');
        $consulta->bindParam(':id', $id);
        $consulta->execute();
        $registros = $consulta->fetchAll();
        return $registros;
    }
    public static function findD($id){
        $conexion = new Conexion();
        $consulta = $conexion->prepare('SELECT * FROM ' . self::TABLA . ' WHERE analysi_id= :id AND type_id =3 ORDER BY id');
        $consulta->bindParam(':id', $id);
        $consulta->execute();
        $registross = $consulta->fetchAll();
        return $registross;
    }
}
